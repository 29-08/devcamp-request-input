//import thu vien express js 
const express = require('express');

//Khoi tao app Express 
const app = express();

//Khai bao cong chay project
const port = 3000;

//Khai bao de app doc duoc body json
app.use(express.json());

//Callback function la mot function tham so cura mot function khac, no se thuc hien khi function day duoc goi
//Khai bao api dang /
app.get("/", (req, res) => {
    let today = new Date();

    res.json({
        message:  `Xin chao, hom nay la ngay ${today.getDate()} thang ${today.getMonth()} nam ${today.getFullYear()}`
    })
})

//Request Params 
//Request params la nhung tham so xuat hien trong URL cua API
app.get("/request-params/:param1/:param2/param", (req, res) => {
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    res.json({
        param: {
            param1,
            param2
        }
    })
})

//Request Query ( chi ap dung cho phuong thuc GET)
//Khi lay query phai validate
app.get("/request-query" , (req,res) => {
    let query = req.query;

    res.json ({
        query
    })
})

//Request Body JSON (chi dung cho POST, PUT)
//Khi lay requets body pahi validate
app.post("/request-body-json", (req,res) => {
    let body = req.body;

    res.json({
        body: body
    })
})

app.listen(port, () => {
    console.log('App listening on port', port);
})